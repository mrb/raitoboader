# -*- coding: utf-8 -*-
import settings

import os
import logging
import urllib
import mimetypes
import uuid

from collections import namedtuple
from datetime import datetime, timedelta

from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound, HttpResponseForbidden
from django.template import RequestContext, Context, Template
from django.shortcuts import render
from django.core.urlresolvers import reverse

from models import File

max_file_age = timedelta(days=30)

#
# Types
#

FileEntry = namedtuple('FileEntry', ('filename', 'size', 'date', 'metadata'))

#
# Private stuff
#

def _path(filename):
    return os.path.join(settings.STORAGE_DIRECTORY, filename)

def _fetch_file_list():
    def mk_file_entry(filename):
        stat = os.stat(_path(filename))
        mtime = int(stat.st_mtime)
        size = stat.st_size

        file_metadata_query = File.objects.filter(filename=filename)[:1]
        file_metadata = file_metadata_query[0] if file_metadata_query else None
        
        return FileEntry(filename, size, mtime, file_metadata)

    files = os.listdir(settings.STORAGE_DIRECTORY)

    return sorted([mk_file_entry(filename) for filename in files], key=lambda fe: fe.date, reverse=True)

def _cull_files(files):
    cutoff_date = datetime.utcnow() - max_file_age
    deleted_filenames = []
    
    for i in xrange(len(files) - 1, -1, -1):
        entry = files[i]
        if datetime.utcfromtimestamp(entry.date) < cutoff_date:
            deleted_filenames.append(entry.filename)
            path = _path(entry.filename)
            try:
                logging.debug(u"Remove old file {}".format(path))
                os.unlink(path)
            except:
                logging.exception(u"Couldn't unlink {}".format(path))
                
            del files[i]
            
    delete_files = File.objects.filter(filename__in=deleted_filenames).delete()
    
    
def _get_file_list():
    files = _fetch_file_list()
    _cull_files(files)
    return files


#
# Request handlers
#

def home(request):
    who = request.session.setdefault('who', str(uuid.uuid4()))
    
    c = {'files': _get_file_list()}
    c['deletable_files'] = [f.filename for f in c['files']
                            if f.metadata is not None and
                            f.metadata.uploaded_by == who]
    
    if 'msg' in request.GET:
        c['msg'] = request.GET['msg']
    return render(request, u'home.html', c)

class _BadUpload(Exception):
    def __init__(self, msg):
        super(_BadUpload, self).__init__(msg)
        self.msg = msg

def _handle_upload(request):
    if request.method != 'POST' or not request.FILES or not 'file' in request.FILES:
       raise _BadUpload(u"Forgot to include a file??")

    uploaded_file = request.FILES['file']
    filename = os.path.basename(uploaded_file.name)
    if uploaded_file.size > 1024 * 1024 * settings.MAX_UPLOAD_SIZE_MB:
        raise _BadUpload(u"That file's WAY too big dude.  Try something smaller than {} MiB.".format(settings.MAX_UPLOAD_SIZE_MB))

    base, ext = os.path.splitext(filename)
    if ext and ext.lower() not in settings.ALLOWED_FILE_EXTENSIONS:
        # blacklisted extension
        filename = base + u'.dat'

    path = _path(filename)
    if os.path.exists(path):
        raise _BadUpload(u"A file already has that name!  Try again, chuckles.")

    with open(path, 'wb') as of:
        of.write(uploaded_file.read())

    file_metadata = File(filename=filename,
                         description=request.POST['description'],
                         delete_password=request.POST['delete_password'],
                         uploaded_by=request.session['who'])
    file_metadata.save()
        
def upload(request):
    request.session.setdefault('who', str(uuid.uuid4()))
    
    try:
        _handle_upload(request)
    except _BadUpload, be:
        return HttpResponseRedirect(reverse('home') + u'?msg={}'.format(urllib.quote(be.msg)))
    except:
        logging.exception(u'Exception in upload handler')
    return HttpResponseRedirect(reverse('home'))
            

def sendfile(request, filename):
    base_filename = os.path.basename(filename)
    path = _path(base_filename)

    if not os.path.exists(path):
        return HttpResponseNotFound(u'not found')
    
    mime, encoding = mimetypes.guess_type(base_filename, False)
    with open(path, 'rb') as inf:
        return HttpResponse(inf.read(), mime)
    
def delete(request):
    who = request.session.setdefault('who', str(uuid.uuid4()))
    
    base_filename = os.path.basename(request.POST['filename'])
    path = _path(base_filename)
    
    file_metadata_query = File.objects.filter(filename=base_filename)[:1]
    if not file_metadata_query:
        return HttpResponseRedirect(reverse('home') + u"?msg={}".format(urllib.quote("That file doesn't exist! What!")))

    metadata = file_metadata_query[0]
    if metadata.uploaded_by != who:
        return HttpResponseRedirect(reverse('home') + u"?msg={}".format(urllib.quote("You didn't upload that! You can't delete it! Go away!")))

    try:
        logging.debug(u"Remove file by request: {}".format(path))
        os.unlink(path)
        file_metadata_query.delete()
    except:
        logging.exception(u"Couldn't unlink {}".format(path))
    
    return HttpResponseRedirect(reverse('home'))

from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
                       url(r'^$', 'raitoboader.views.home', name='home'),
                       url(r'^upload$', 'raitoboader.views.upload', name='upload'),
                       url(r'^get/([^/]+)$', 'raitoboader.views.sendfile', name='sendfile'),
                       url(r'^delete$', 'raitoboader.views.delete', name='delete'),
                       
                       url(r'^admin/', include(admin.site.urls)),
                   )

from django.db import models

class File(models.Model):
    filename = models.TextField()
    description = models.TextField()
    delete_password = models.CharField(max_length=200)
    uploaded_by = models.CharField(max_length=36) # uuid.uuid4()
    
    
    
